using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace app1.manager
{
    public abstract class ProduseAbstractManager
    {
        protected List<ProdusAbstract> _elemente = new List<ProdusAbstract>();

        public List<ProdusAbstract> Elemente
        {
            get => _elemente;
            set => _elemente = value;
        }

        public void Write2Console()
        {
            foreach (var element in _elemente.OrderBy(o => o.Nume))
            {
                Console.WriteLine(element.ToString());
            }
        }

        protected void CheckDuplicate(ProdusAbstract produsAbstract)
        {
            if (_elemente.Contains(produsAbstract))
            {
                Console.WriteLine("Produsul exista deja");
            }
            else
            {
                _elemente.Add(produsAbstract);
            }
        }

        public List<T> DeserializeFromXml<T>(string fileName, string rootElement)
        {
            var xml = XDocument.Load(@"..\..\..\xml\" + fileName);
            var serializer = new XmlSerializer(typeof(List<T>), new XmlRootAttribute(rootElement));
            var stringReader = new StringReader(xml.ToString());
            var productList = serializer.Deserialize(stringReader);

            return (List<T>) productList;
        }
    }
}