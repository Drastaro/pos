﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace app1.manager
{
    public class PachetManager : ProduseAbstractManager
    {
        private static List<Pachet> packets = new List<Pachet>();

        public void ReadPachet(string fileName, string rootElement)
        {
            var produseManager = new ProduseManager();
            var productList = produseManager.DeserializeFromXml<Produs>(fileName, rootElement);
            produseManager.Elemente.AddRange(productList);

            var packet = createPacket(productList, produseManager);
            packets.Add(packet);
        }

        private Pachet createPacket(List<Produs> productList, ProduseManager produseManager)
        {
            Console.WriteLine("Introdu un Pachet");
            Console.Write("Codul intern:");
            var codIntern = Console.ReadLine();
            Console.Write("Numele:");
            var nume = Console.ReadLine();

            var sum = productList.Sum(item => item.Pret);
            return new Pachet(_elemente.Count + 1, codIntern, nume, sum, produseManager.Elemente);
        }

        public void PrintPacket()
        {
            foreach (var element in packets.OrderBy(o => o.Pret))
            {
                element.Print();
            }
        }
    }
}