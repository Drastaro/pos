﻿using System;
using System.Collections.Generic;

namespace app1.manager
{
    public class ServiciiManager : ProduseAbstractManager
    {
        public void ReadServiciu()
        {
            Console.WriteLine("Introdu un produs");
            Console.Write("Codul intern:");
            var codIntern = Console.ReadLine();
            Console.Write("Numele:");
            var nume = Console.ReadLine();
            Console.Write("Pret:");
            var pret = Convert.ToDouble(Console.ReadLine());
            var serviciu = new Serviciu(_elemente.Count + 1, codIntern, nume, pret);
            CheckDuplicate(serviciu);
        }

        public void ReadServicii(int nr)
        {
            for (var cnt = 0; cnt < nr; cnt++)
            {
                Console.WriteLine("Introdu un serviciu");
                Console.Write("Codul intern:");
                var codIntern = Console.ReadLine();
                Console.Write("Numele:");
                var nume = Console.ReadLine();
                Console.Write("Pret:");
                var pret = Convert.ToDouble(Console.ReadLine());
                var serviciu = new Serviciu(_elemente.Count + 1, codIntern, nume, pret);
                CheckDuplicate(serviciu);
            }
        }

        public void WriteServicii()
        {
            foreach (var serviciu in _elemente)
            {
                Console.WriteLine(serviciu.ToString());
            }
        }
    }
}