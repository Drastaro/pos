﻿using System;
using System.Collections.Generic;

namespace app1.manager
{
    public class ProduseManager : ProduseAbstractManager
    {
        public void ReadProdus()
        {
            Console.WriteLine("Introdu un produs");
            Console.Write("Codul intern:");
            var codIntern = Console.ReadLine();
            Console.Write("Numele:");
            var nume = Console.ReadLine();
            Console.Write("Producator:");
            var producator = Console.ReadLine();
            Console.Write("Pret:");
            var pret = Convert.ToDouble(Console.ReadLine());
            var produs = new Produs(_elemente.Count + 1, nume, codIntern, producator, pret);
            CheckDuplicate(produs);
        }

        public void ReadProduse(int nr)
        {
            for (var cnt = 0; cnt < nr; cnt++)
            {
                Console.WriteLine("Introdu un produs");
                Console.Write("Codul intern:");
                var codIntern = Console.ReadLine();
                Console.Write("Numele:");
                var nume = Console.ReadLine();
                Console.Write("Producator:");
                var producator = Console.ReadLine();
                Console.Write("Pret:");
                var pret = Convert.ToDouble(Console.ReadLine());
                var produs = new Produs(_elemente.Count + 1, codIntern, nume, producator, pret);
                CheckDuplicate(produs);
            }
        }

        public void WriteProduse()
        {
            foreach (var produs in _elemente)
            {
                Console.WriteLine(produs.ToString());
            }
        }
    }
}