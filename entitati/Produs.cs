using System;
using System.Linq;

namespace app1
{
    public class Produs : ProdusAbstract, IPackageble
    {
        private string _producator;

        public Produs()
        {
        }

        public Produs(long id, string codIntern, string nume, string producator, double pret) : base(id, codIntern,
            nume, pret)
        {
            _producator = producator;
        }

        public string Producator
        {
            get => _producator;
            set => _producator = value;
        }

        public bool CanAddToPackage(Pachet pachet)
        {
            return true;
        }

        public override string ToString()
        {
            return $"Produs: Id: {Id}, CodIntern: {CodIntern}, Nume: {Nume}, Producator: {_producator}, Pret: {Pret}";
        }

        public override string Descriere()
        {
            return ToString();
        }

        public override string AltaDescriere()
        {
            return "Produsul" + base.AltaDescriere() + " Producator: " + _producator;
        }
    }
}