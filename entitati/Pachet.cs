﻿using System;
using System.Collections.Generic;

namespace app1
{
    public class Pachet : ProdusAbstract
    {
        private List<ProdusAbstract> elem_pachet = new List<ProdusAbstract>();

        public Pachet(long id, string codIntern, string nume, double pret, List<ProdusAbstract> elemPachet) : base(id,
            codIntern, nume, pret)
        {
            elem_pachet.AddRange(elemPachet);
        }

        public override string ToString()
        {
            return
                $"Packet: Id: {Id}, CodIntern: {CodIntern}, Nume: {Nume}, Pret: {Pret}, Elemente:";
        }

        public override string Descriere()
        {
            return ToString();
        }

        public void Print()
        {
            Console.WriteLine(ToString());
            PrintElemente();
        }

        public void PrintElemente()
        {
            foreach (var element in elem_pachet)
            {
                Console.WriteLine("\t" + element.ToString());
            }
        }
    }
}