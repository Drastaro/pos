namespace app1
{
    public class Serviciu : ProdusAbstract, IPackageble
    {
        public Serviciu()
        {
        }

        public Serviciu(long id, string codIntern, string nume, double pret) : base(id, codIntern, nume, pret)
        {
        }

        public bool CanAddToPackage(Pachet pachet)
        {
            return true;
        }

        public override string ToString()
        {
            return $"Serviciu: Id: {Id}, CodIntern: {CodIntern}, Nume: {Nume}, Pret: {Pret}";
        }

        public override string Descriere()
        {
            return ToString();
        }

        public override string AltaDescriere()
        {
            return "Serviciu" + base.AltaDescriere();
        }
    }
}