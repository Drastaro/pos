﻿using System.Collections;
using System.Collections.Generic;

namespace app1
{
    public class ListaGen<T> : IEnumerable
    {
        private Nod _start;
        private Nod _end;

        public ListaGen()
        {
            _start = null;
            _end = null;
        }

        private class Nod
        {
            private T _data;
            private Nod _next;

            public Nod(T data)
            {
                _data = data;
            }

            public T Data
            {
                get => _data;
                set => _data = value;
            }

            public Nod Next
            {
                get => _next;
                set => _next = value;
            }
        }

        public void InsertToFront(T t)
        {
            var n = new Nod(t) {Next = _start};
            _start = n;
            if (_end == null) _end = n;
        }

        public void InsertToEnd(T t)
        {
            var n = new Nod(t);
            if (_start == null) _start = n;
            if (_end != null) _end.Next = n;
            _end = n;
        }

        public void delete(T t)
        {
            Nod current = _start;
            while (current != null && current.Data.Equals(t))
            {
                _start = _start.Next;
                current = _start;
            }

            if (current == null) return;
            while (current.Next != null)
            {
                if (current.Next.Data.Equals(t))
                {
                    if (_end == current.Next)
                    {
                        _end = current;
                        _end.Next = null;
                    }
                    else
                    {
                        var delNod = current.Next;
                        current.Next = current.Next.Next;
                        delNod.Next = null;
                    }
                }

                else current = current.Next;
            }
        }

        public IEnumerator GetEnumerator()
        {
            var current = _start;
            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }
    }
}