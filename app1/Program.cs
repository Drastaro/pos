﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using app1.manager;

namespace app1
{
    class Program
    {
        private static void Main(string[] args)
        {
            // produseManager.Write2Console();

            // var serviciiManager = new ServiciiManager();
            // var servicii = serviciiManager.ReadFromXml<Serviciu>("servicii.xml", "servicii");
            // serviciiManager.Elemente.AddRange(servicii);
            // serviciiManager.Write2Console();

            // var linqProdus = from prod in productList
            //     where prod.Producator == "CHINA"
            //     select prod;
            // foreach (var linq in linqProdus)
            // {
            //     Console.WriteLine("\n" + linq);
            // }

            // var listaGenerica = new ListaGen<ProdusAbstract>();
            // listaGenerica.InsertToFront(new Produs());
            // listaGenerica.InsertToEnd(new Serviciu());

            // foreach (var o in listaGenerica)
            // {
            //     Console.WriteLine(o);
            // }

            var pachetManager = new PachetManager();
            pachetManager.ReadPachet("produse.xml", "produse");
            pachetManager.ReadPachet("produse2.xml", "produse");
            pachetManager.ReadPachet("servicii.xml", "servicii");
            pachetManager.PrintPacket();
        }
    }
}