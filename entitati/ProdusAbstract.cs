﻿using System;

namespace app1
{
    public abstract class ProdusAbstract
    {
        private long _id;
        private string _codIntern;
        private string _nume;
        private double _pret;

        protected ProdusAbstract()
        {
        }

        protected ProdusAbstract(long id, string codIntern, string nume, double pret)
        {
            _id = id;
            _codIntern = codIntern;
            _nume = nume;
            _pret = pret;
        }

        public long Id
        {
            get => _id;
            set => _id = value;
        }

        public string CodIntern
        {
            get => _codIntern;
            set => _codIntern = value;
        }

        public string Nume
        {
            get => _nume;
            set => _nume = value;
        }

        public double Pret
        {
            get => _pret;
            set => _pret = value;
        }

        public override bool Equals(object obj)
        {
            return CodIntern.Trim().ToLower().Equals(((ProdusAbstract) obj)?.CodIntern.Trim().ToLower()) ||
                   Nume.Trim().ToLower().Equals(((ProdusAbstract) obj)?.Nume.Trim().ToLower());
        }

        public override int GetHashCode()
        {
            return Nume.GetHashCode();
        }

        public abstract string Descriere();

        public virtual string AltaDescriere()
        {
            return "[" + Id + "]: " + Nume
                   + " cod intern :[" + CodIntern + "] "
                   + " pret :[" + Pret + "] ";
        }
    }
}